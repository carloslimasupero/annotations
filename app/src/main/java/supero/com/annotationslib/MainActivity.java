package supero.com.annotationslib;

        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.widget.Button;

        import org.androidannotations.annotations.AfterViews;
        import org.androidannotations.annotations.Click;
        import org.androidannotations.annotations.EActivity;
        import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.button)
    protected Button mTela;

    @AfterViews
    protected void init()
    {

    }

    @Click(R.id.button)
    protected void onClickAbrirTela()
    {
        SecondActivity_.intent(this).start();
    }
}
